.globl app
app:
	// X0 contiene la direccion base del framebuffer

	//---------------- CODE HERE ------------------------------------


 	mov x2,512         // Y Size
loop1:
	mov x1,512         // X Size
	mov w10, 0x001F    // 0xF800 = RED
	mov w1,0x0000		// Azul
	mov w2,0x0000     // Verde
loop0:
		//cambia los colores
		sub w11,w10,0x001F
		cbnz w11,else1
		sub w11,w2,0x003F
		cbz w11,elsebluem
		add w2,w2,1
		b end
elsebluem:
		sub w1,w1,0x0001
		b end
else1:
		sub w11,w2,0x003F
		cbnz w11,else2
		sub w11,w1,wzr
		cbnz w11,elseblueM
		sub w10,w10,0x0001
		b end
elseblueM:
		add w1,w1,0x0001
		b end
else2:
		sub w11,w1,0x001F
		cbnz w11,end
		sub w11,w2,wzr
		cbz w11,elseredM
		sub w2,w2,1
		b end
elseredM:
		add w10,w10,0x0001
		b end
end:
		lsl w10,w10,0x000B
		lsl w2,w2,0x0005
		orr w10,w10,w2
		orr w10,w10,w1

	sturh w10,[x0]	   // Set color of pixel N
	add x0,x0,2	   // Next pixel
	sub x1,x1,1	   // decrement X counter
	cbnz x1,loop0	   // If not end row jump
	sub x2,x2,1	   // Decrement Y counter
	cbnz x2,loop1	   // if not last row, jump

	//---------------------------------------------------------------

        // Infinite Loop
InfLoop:
	b InfLoop
